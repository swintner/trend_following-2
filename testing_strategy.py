from strategy import *


bp = get_market_data_using_merge_ordered('british_pound')
fix_quotes(bp, 50, price_fields='Close')
plt.figure(figsize=(14, 8))
plt.plot(bp['Close'], linewidth=0.5)
plt.title('Pound Merge_Ordered')
plt.legend([])
plt.show()
print('Data Head:',  bp.head(), sep='\n')
print('\n\nData Tail:', bp.tail(), sep='\n')
print('\n\nData Info:')
bp.info()  # Info does not like to be called in print line?
print('\n\nData Describe:', bp.describe(), sep='\n')


bp2 = get_market_data_using_concat('british_pound')
bp2['Close'] = fix_quotes(bp2, 50, price_fields='Close')
bp2 = bp2[['Close', 'Volume', 'OpenInt', 'Symbol']]
plt.figure(figsize=(14, 8))
plt.plot(bp2['Close'], linewidth=0.5)
plt.title('Pound Concat')
plt.legend([])
plt.show()
print('Data Head:',  bp2.head(), sep='\n')
print('\n\nData Tail:', bp2.tail(), sep='\n')
print('\n\nData Info:')
bp2.info()  # Info does not like to be called in print line?
print('\n\nData Describe:', bp2.describe(), sep='\n')

