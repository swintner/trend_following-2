import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import itertools as it

from covel import covel_directory_driver

plt.style.use('seaborn')


def get_market_data_using_merge_ordered(market_symbol, endswith_field='.txt'):
    directory = covel_directory_driver(market_symbol)
    list_of_files = [file for file in os.listdir(directory) if file.endswith(endswith_field)]
    list_of_dfs = [prep_covel(directory + filename, filename) for filename in list_of_files]
    names = [file.replace('.txt', '') for file in list_of_files]
    df = list_of_dfs[0]
    df['Symbol'] = names[0]
    for contract, name in zip(list_of_dfs[1:], names[1:]):
        contract['Symbol'] = name
        df = pd.merge_ordered(df, contract)
    df = df.loc[df.groupby('Date')['OpenInt'].idxmax(), ['Date', 'Close', 'OpenInt', 'Symbol']]
    df = df.set_index('Date')
    return df


def get_market_data_using_concat(market_symbol, endswith_field='.txt'):
    directory = covel_directory_driver(market_symbol)
    list_of_files = [file for file in os.listdir(directory) if file.endswith(endswith_field)]
    df = pd.concat([prep_covel(directory + filename, filename) for filename in list_of_files])  # <--is covel specific, must be changed to accommodate other dir
    df = df.sort_values('Date')
    return df.set_index('Date')


def prep_covel(relative_path, filename):
    # prep data: step 1, process files with headers. Step 2, process files w/o headers and dates as ints.
    try:
        df = pd.read_csv(relative_path).assign(Symbol=filename.replace('.txt', ''))
        # you could try to add a check here whether the date column contains "/" and how long the value is (this will help with the 2000 problem)
        df['Date'] = pd.to_datetime(df['Date'], format='%m/%d/%Y')
    except KeyError:
        # converters={'Date': lambda x: str(x)} prevents 3 leading zeros being dropped from 000101 (Jan 1, 2000)
        df = pd.read_csv(relative_path,
                         names=['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'OpenInt'], converters={'Date': lambda x: str(x)}). \
            assign(Symbol=filename.replace('.txt', ''))
        df['Date'] = pd.to_datetime(df['Date'], format='%y%m%d')
    # adjust for date mapping issues: values 69–99 are mapped to 1969–1999, and values 0–68 are mapped to 2000–2068.
    df.loc[df['Date'].dt.year >= 2058, 'Date'] = df.loc[df['Date'].dt.year >= 2058, 'Date'] - np.timedelta64(100, 'Y')

    return df


def show_plot(input_data, market_name='Pound', field_name='Close'):
    plt.figure(figsize=(14, 8))
    plt.plot(input_data[field_name], linewidth=0.5)
    plt.title(f'{market_name} {field_name}')
    plt.legend([])
    return plt.show()


def save_plot(input_data, market_name, field_name='Close', directory_name='', img_type='.png'):  # leaving directory name as empty string and img_type as .png will default save to pwd
    fields = [i for i in input_data.columns if f'{field_name}_' in i]
    plt.figure(figsize=(14, 8))
    plt.plot(input_data[fields], linewidth=0.5)
    plt.title(f'{market_name} {field_name}')
    plt.legend([])
    return plt.savefig(f'{directory_name}' + f'{market_name}' + f'{field_name}' + f'{img_type}')


def fix_quotes(input_data, price_limit, price_fields='Close'):

    input_data[price_fields] = np.where(input_data[price_fields] > price_limit, (1 / input_data[price_fields]) * 100, (1 / input_data[price_fields]))

    return input_data[price_fields]


def run_strategy(input_data):

    def create_strategy(input_data, fast_ma, slow_ma, input_data_closes):
        input_data['fast_ma'] = input_data[input_data_closes].ewm(span=fast_ma)
        input_data['slow_ma'] = input_data[input_data_closes].ewm(span=slow_ma)
        # while the next line is true, it will not work for continous data
        input_data['Log_Ret'] = np.log(input_data[input_data_closes] / input_data[input_data_closes].shift(1))
        input_data['Position'] = np.where(input_data[fast_ma] > input_data[slow_ma], 1, -1)
        input_data['Returns'] = np.log(input_data[input_data_closes] / input_data[input_data_closes].shift(1)) # <-- get rid of input_data_closes, should be 1 close column
        input_data['Strategy'] = input_data['Position'].shift(1) * input_data['Returns']
        # trades = (input_data['Position'].diff().fillna(0) != 0) <-- keep for transaction costs
        # input_data['Strategy'] = np.where(trades, input_data['Strategy'] - self.tc, input_data['Strategy']) <-- keep for transaction costs
        input_data['CReturns'] = input_data['Returns'].cumsum().apply(np.exp)
        input_data['CStrategy'] = input_data['Strategy'].cumsum().apply(np.exp)
        return input_data[['CReturns', 'CStrategy']].iloc[-1]

    for fast_ma, slow_ma in it.product(range(12, 116, 1), range(26, 252, 1)):
    # this would result in SMA1 being longer dated than SMA2 is this intended?
        # print(SMA1, SMA2)
        res = create_strategy(input_data, fast_ma, slow_ma, input_data['Close'])
        # print(res)
        results = results.append(pd.DataFrame({'Fast': fast_ma, 'Slow': slow_ma, 'CReturns': res['CReturns'],
                                               'CStrategy': res['CStrategy']}, index=[0]), ignore_index=True)

    annual_vol = input_data[['CReturns', 'CStrategy']].std() * 252 ** 0.5
    ema_pair = results['CStrategy'].idxmax()

    return f'The annual volatility is {annual_vol} and the EMA pair with the highest return is {ema_pair}'
