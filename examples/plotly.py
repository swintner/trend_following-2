# cannot be used until continuous contracts are made
def plotly_plot(input_data, market_name=str, field_name=str):
    fields = [i for i in input_data.columns if f'{field_name}_' in i]
    input_data['Date'] = input_data.index
    fig = px.line(input_data[fields], x='Date')
    return fig.write_html(f'{market_name}.html', auto_open=True)