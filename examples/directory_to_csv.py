from strategy import *

# leave for reference DO NOT USE MULTIPLE TIMES, TAKES ~13 MINUTES TO PROCESS ALL MARKETS TO CSVs
def all_markets_to_combined_dfs(main_directory_relative_path):
    # this was done to automate testing of all, any date errors: errors will default to 1970-01-01 00:00:00 and be vertical on time series
    list_of_markets = [file for file in os.listdir(f'{main_directory_relative_path}')]
    bigg = [get_market_data(market) for market in list_of_markets]
    print('starting')
    for market, little in zip(list_of_markets, bigg):
        little.to_csv(f"data/combined_dfs/{market}")